<!--Header Include-->
<?php $this->load->view('admin/includes/common-header'); ?>
<!--/Header Include-->



	<div class="content-container">
            <div class="container-fluid">
                 <div class="row">	
					 <h3>Projects</h3>
		         </div>
            </div>
			<div class="bd-example">
			            
						<div class = "table-responsive">
							   <table class = "table table-bordered table-striped">
								  
								  <thead>
									 <tr>
										<th>Name</th> 
										<th>View</th>		
									 </tr>
								  </thead>
								  
								  <tbody>
								        <?php foreach($result as $row){ ?>
											 <tr>
												<td class="capitalize"><?php echo $row['name']; ?></td>
												<td><a href="<?php echo base_url(); ?>admin/projects/view/<?php echo $row['projectId']; ?>">View</a></td>
											 </tr>
										<?php } ?>	 
								  </tbody>
								  
							   </table>
							</div>  
							<nav aria-label="Page navigation">
							     <ul class="pagination">
							         <li class="page-item"><?php echo $pagination ?></li>
							     </ul>
							</nav>	 
			</div>
	</div>

<!--Footer Include-->
<?php $this->load->view('admin/includes/common-footer'); ?>
<!--/Footer Include-->