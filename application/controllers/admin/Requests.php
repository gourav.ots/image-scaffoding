<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Requests extends CI_Controller {
	
	public function __construct(){
		parent::__construct();
		$this->load->library('session');
		$this->load->helper('url');
	}
	
	public function index()
	{
          $this->load->library('pagination');
		  $config['base_url'] = base_url().'admin/requests/';
		  $config['total_rows'] =  $this->db->get_where('projectscordinatesrequest', array('status'=> 1))->num_rows();
		  $config['per_page'] = 15;
		  $config['page_query_string'] = TRUE;
		  $config['query_string_segment'] = 'page';
		  
		  $page = (isset($_GET['page'])) ? $_GET['page'] : 0;
		  
		  $this->pagination->initialize($config);
		  
		  $data["pagination"] = $this->pagination->create_links();
		  
		  $data['result'] =  $this->db->join('projects', 'projects.projectId = projectscordinatesrequest.projectId')->get_where('projectscordinatesrequest', array('projectscordinatesrequest.status'=> 1), $config["per_page"], $page)->result_array();
		  $this->load->view('admin/requests/listing', $data);
	}
	
 
	
    public function delete(){
		$type_decrypted_key = str_replace(" ", '+', $this->input->get('id'));
		$id = mc_decrypt($type_decrypted_key, ENCRYPTION_KEY);
		
		$data = array(
			'status'=> '2'
		);
	    $this->db->where('projectCordinateRequestId', $id);		 		 
	    $this->db->update('projectscordinatesrequest', $data);	
		
		$this->session->set_flashdata('message','<div class="alert alert-danger alert-notification"><b>Congratulation</b> Successfully deleted</div>');
		redirect('admin/requests');
	} 


}
