<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Projects extends CI_Controller {
	
	public function __construct(){
		parent::__construct();
		$this->load->library('session');
		$this->load->helper('url');
	}
	
	public function index()
	{
          $this->load->library('pagination');
		  $config['base_url'] = base_url().'admin/projects/';
		  $config['total_rows'] =  $this->db->get_where('projects', array('status'=> 1))->num_rows();
		  $config['per_page'] = 15;
		  $config['page_query_string'] = TRUE;
		  $config['query_string_segment'] = 'page';
		  
		  $page = (isset($_GET['page'])) ? $_GET['page'] : 0;
		  
		  $this->pagination->initialize($config);
		  
		  $data["pagination"] = $this->pagination->create_links();
		  
		  $data['result'] =  $this->db->get_where('projects', array('status'=> 1), $config["per_page"], $page)->result_array();
		  $this->load->view('admin/projects/listing', $data);
	}
	
	public function view($projectId)
	{
		  $data['project'] = $this->db->get_where('projects', array('projectId'=> $projectId))->row_array();
		  $data['settings'] = $this->db->get_where('settings')->row_array();
		  $this->load->view('admin/projects/view', $data);
	}
	
	public function saveProjectsCordinate(){
         $img = $_POST['img_val'];
		 $img = str_replace('data:image/png;base64,', '', $img);
	     $img = str_replace(' ', '+', $img);
	     $dataimg = base64_decode($img);
		
		 $nameimg = uniqid();
         $fileimg = 'uploads/cordinateImages/'. $nameimg . '.png';
		 $successimg = file_put_contents($fileimg, $dataimg);
		 
		 $findByCordinates = $this->db->get_where('projectscordinates', array("deck" => $_POST['deck'], "block"=> $_POST['block'], "side"=> $_POST['side']))->row_array();
		 
		 if(empty($findByCordinates)){
			 $data = array(
				  "projectId" => $_POST['projectId'],
				  "image" => $nameimg.'.png',
				  "deck" => $_POST['deck'],
				  "block" => $_POST['block'],
				  "side" => $_POST['side']
			  ); 
			  $this->db->insert('projectscordinates', $data);
			  
			  $_SESSION['deck'] = "";
			  $_SESSION['block'] = "";
			  $_SESSION['side'] = "";
			  
			  $this->session->set_flashdata('message','<div class="alert alert-success alert-notification"><b>Congratulation</b> Successfully saved.</div>');
			  redirect($_SERVER['HTTP_REFERER'],'refresh'); 
		}else{
			 $data = array(
				  "projectId" => $_POST['projectId'],
				  "image" => $new_img_name,
				  "deck" => $_POST['deck'],
				  "block" => $_POST['block'],
				  "side" => $_POST['side']
			  ); 
			  $this->db->where('deck', $_POST['deck']);
			  $this->db->where('block', $_POST['block']);
			  $this->db->where('side', $_POST['side']);
			  $this->db->update('projectscordinates', $data);
			  
			  /* Remove last created file from folder */
			  $path_to_file = $findByCordinates['image'];
			  $path_to_file = FCPATH.$path_to_file;   
			  if(unlink($path_to_file)) {
				  echo 'deleted successfully';
			  }
			  
			  $_SESSION['deck'] = "";
			  $_SESSION['block'] = "";
			  $_SESSION['side'] = "";
			  
			  $this->session->set_flashdata('message','<div class="alert alert-success alert-notification"><b>Congratulation</b> Successfully updated.</div>');
			  redirect($_SERVER['HTTP_REFERER'],'refresh'); 
		}	  
	}
	
	public function findProjectsCordinate(){
		 $findByCordinates = $this->db->get_where('projectscordinates', array("deck" => $_POST['deckInput'], "block"=> $_POST['blockInput'], "side"=> $_POST['sideInput']))->row_array();
		 
		 if(empty($findByCordinates)){
			 echo "TRUE";
		 }else{
			 echo "FALSE";
		 }	 
	}	 
	
    public function saveCroppedImage(){
		 $img = $_POST['image'];
		 $img = str_replace('data:image/png;base64,', '', $img);
	   
		 $img = str_replace(' ', '+', $img);
	   
		 $dataimg = base64_decode($img);
		
		 $nameimg = uniqid() ;

		 $fileimg = 'uploads/cropedImages/'. $nameimg . '.jpg';
		 $successimg = file_put_contents($fileimg, $dataimg);
		 echo $nameimg; 
	} 	 
	
/* 	public function delete(){
		$type_decrypted_key = str_replace(" ", '+', $this->input->get('id'));
		$id = mc_decrypt($type_decrypted_key, ENCRYPTION_KEY);
		
		$data = array(
			'status'=> '0'
		);
	    $this->db->where('id', $id);		 		 
	    $this->db->update('units', $data);	
		
		$this->session->set_flashdata('message','<div class="alert alert-danger"><b>Congratulation</b> Successfully deleted</div>');
		redirect('admin/units/listing');
	} */


}
