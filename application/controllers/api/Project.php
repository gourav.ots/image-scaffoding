<?php defined('BASEPATH') OR exit('No direct script access allowed');

require APPPATH . 'libraries/REST_Controller.php';

class Project extends REST_Controller {

    function __construct()
    {
        parent::__construct();
    }

	//http://192.168.1.175/image_scaffholding_1/api/project/images?projectId=3&deck=100&block=100&&side=100


    public function images_get()
    {
        $projectId = $this->get('projectId');
        $deck = $this->get('deck');
        $block = $this->get('block');
        $side = $this->get('side');
		
		if(!isset($projectId)){
			$this->response([
                    'status' => FALSE,
                    'message' => 'Project id is missing.'
            ], REST_Controller::HTTP_NOT_FOUND);
		}
		
		if(!isset($deck)){
			$this->response([
                    'status' => FALSE,
                    'message' => 'Deck id is missing.'
            ], REST_Controller::HTTP_NOT_FOUND);
		}
		
		if(!isset($block)){
			$this->response([
                    'status' => FALSE,
                    'message' => 'Block id is missing.'
            ], REST_Controller::HTTP_NOT_FOUND);
		}
				
		if(!isset($side)){
			$this->response([
                    'status' => FALSE,
                    'message' => 'Side id is missing.'
            ], REST_Controller::HTTP_NOT_FOUND);
		}
		
		$findByCordinates = $this->db->get_where('projectscordinates', array("projectId"=> $projectId, "deck" => $deck, "block"=> $block, "side"=> $side))->row_array();
		
		
		 if(empty($findByCordinates)){
			 /* Inserting new requesting cordinates */
			 $findByCordinatesrequest = $this->db->get_where('projectscordinatesrequest', array("projectId"=> $projectId, "deck" => $deck, "block"=> $block, "side"=> $side))->row_array();
			 if(empty($findByCordinatesrequest)){
				 $data = array(
					  "projectId" => $projectId,
					  "deck" => $deck,
					  "block" => $block,
					  "side" => $side
				 ); 
				 $this->db->insert('projectscordinatesrequest', $data);
			 }	 
			 /* ** */
			 
			 $this->response([
						'status' => FALSE,
						'message' => 'Image is not existing in our system with this cordinates.'
				], REST_Controller::HTTP_NOT_FOUND);
		 }else{
			    $this->response([
						'status' => FALSE,
						'image' => base_url().$findByCordinates['image'],
						'message' => 'Final image is this.'
				], REST_Controller::HTTP_OK);
		 }
		


    }



}
