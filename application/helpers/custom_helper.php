<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');
if ( ! function_exists('mc_encrypt')){
   function mc_encrypt($encrypt, $key){
	    $encrypt = serialize($encrypt);
		$iv = mcrypt_create_iv(mcrypt_get_iv_size(MCRYPT_RIJNDAEL_256, MCRYPT_MODE_CBC), MCRYPT_DEV_URANDOM);
		$key = pack('H*', $key);
		$mac = hash_hmac('sha256', $encrypt, substr(bin2hex($key), -32));
		$passcrypt = mcrypt_encrypt(MCRYPT_RIJNDAEL_256, $key, $encrypt.$mac, MCRYPT_MODE_CBC, $iv);
		$encoded = base64_encode($passcrypt).'|'.base64_encode($iv);
		return $encoded;
	   
   }
}


if ( ! function_exists('mc_decrypt')){
   function mc_decrypt($decrypt, $key){
	   
	    $decrypt = explode('|', $decrypt.'|');
		$decoded = base64_decode($decrypt[0]);
		$iv = base64_decode($decrypt[1]);
		if(strlen($iv)!==mcrypt_get_iv_size(MCRYPT_RIJNDAEL_256, MCRYPT_MODE_CBC)){ return false; }
		$key = pack('H*', $key);
		$decrypted = trim(mcrypt_decrypt(MCRYPT_RIJNDAEL_256, $key, $decoded, MCRYPT_MODE_CBC, $iv));
		$mac = substr($decrypted, -64);
		$decrypted = substr($decrypted, 0, -64);
		$calcmac = hash_hmac('sha256', $decrypted, substr(bin2hex($key), -32));
		if($calcmac!==$mac){ return false; }
		$decrypted = unserialize($decrypted);
		return $decrypted;
	   
   }
}

if ( ! function_exists('csrf_token')){
   function csrf_token(){
	    $CI =& get_instance();
		$CI->load->library("session");
		//$csrf_key=rand(10,"10000000000000000");
		//$CI->session->set_userdata('csrf',$csrf_key);
		//return $decrypted;
	   
   }
}



if ( ! function_exists('check_operating_system')){
   function check_operating_system(){
		$user_agent = getenv("HTTP_USER_AGENT");
		if(strpos($user_agent, "Win") !== FALSE){
		$os = "Windows";
		}elseif(strpos($user_agent, "Mac") !== FALSE){
		$os = "Mac";
		}
		return $os;
	   
   }
}



?>
