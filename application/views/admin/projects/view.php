<!--Header Include-->
<?php $this->load->view('admin/includes/common-header');  ?>
<!--/Header Include-->

<script src="<?php echo base_url(); ?>resources/html2canvas/html2canvas.js"></script>
<script src="<?php echo base_url(); ?>resources/html2canvas/jquery.plugin.html2canvas.js"></script> 


    <style>
       canvas { box-shadow: 0 0 10px black;}​
    </style>
	
	<script>
	$(document).ready(function() {
		var condition = 1;
		var points = [];//holds the mousedown points
		var canvas = document.getElementById('myCanvas');
		this.isOldIE = (window.G_vmlCanvasManager);
		$(function() {
				if (this.isOldIE) {
					G_vmlCanvasManager.initElement(myCanvas);
				}
				var ctx = canvas.getContext('2d');
				var imageObj = new Image();
				
				function init() {
					canvas.addEventListener('mousedown', mouseDown, false);
					canvas.addEventListener('mouseup', mouseUp, false);
					canvas.addEventListener('mousemove', mouseMove, false);
				}

				imageObj.onload = function() {
					ctx.drawImage(imageObj, 0, 0);
                };
				//imageObj.src = "download.png";
				//imageObj.src = "img.png";
				imageObj.src = "<?php echo base_url(); ?>uploads/shipbuilding-50.jpg";
				ctx.globalCompositeOperation = 'destination-over';
				
				
				$('#myCanvas').mousemove(function(e) {
					if (condition == 1) {
						ctx.beginPath();
						$('#posx').html(e.offsetX);
						$('#posy').html(e.offsetY);
					}
				});
				//mousedown event
				$('#myCanvas').mousedown(function(e) {
				    if (condition == 1) {
					        if (e.which == 1) {
								var pointer = $('<span class="spot">').css({
									'position': 'absolute',
									'background-color': '#e8b200', // pointer color
									'width': '7px',
									'height': '7px',
									'top': e.pageY,
									'left': e.pageX
								});
								
								
								//store the points on mousedown
								points.push(e.pageX, e.pageY);
								//console.log(points);
								ctx.globalCompositeOperation = 'destination-out';
								
								
								<!-- ===================================== -->
								ctx.fillRect(20,20,75,50);
								ctx.globalCompositeOperation="source-over";
								<!-- ======================================= -->
								
								
								var oldposx = $('#oldposx').html();
								var oldposy = $('#oldposy').html();
								var posx = $('#posx').html();
								var posy = $('#posy').html();
								ctx.beginPath();
								ctx.moveTo(oldposx, oldposy);
								if (oldposx != '') {
									ctx.lineTo(posx, posy);
									ctx.stroke();
								}
								$('#oldposx').html(e.offsetX);
								$('#oldposy').html(e.offsetY);
						}
						
						//$(document.body).append(pointer);
						$("#capture-div").append(pointer);
						$('#posx').html(e.offsetX);
						$('#posy').html(e.offsetY);
					}//condition
				});

				$('#crop').click(async function() {
					var spotLength = $('.spot').length;
					var deckInput = $("[name='deck']").find('option:selected').val();
					var blockInput = $("[name='block']").find('option:selected').val();
					var sideInput = $("[name='side']").find('option:selected').val();
					if(spotLength == 0){
						alert("Please select below image cordinates for proceed.");
						return false;
					}else if(spotLength <= 3){
						alert("Cordinates should be atleast 4.");  
						return false;
					}
					
					if(deckInput == ""){
						alert("Please select deck.");
						$("[name='deck']").focus();
						return false;
					} 
					if(blockInput == ""){
						alert("Please select block.");
						$("[name='block']").focus();
						return false;
					} 
					if(sideInput == ""){
						alert("Please select side.");
						$("[name='side']").focus();
						return false;
					}  
					$(this).attr("disabled", "disabled");
					$(this).html("Saving...");
					$('#loading-div').addClass('loading');
                    
                    $('.spot').each(function(){
                         $(this).remove();
				    });					


					// crop draw canvas function
				    await captureImage();
					
					setTimeout(async function() {
					    ctx.clearRect(0, 0, 633, 296);
						var offset = $('#myCanvas').offset();
						
						for (var i = 0; i < points.length; i += 2) {
							var x = parseInt(jQuery.trim(points[i]));
							var y = parseInt(jQuery.trim(points[i + 1]));
							if (i == 0) {
								ctx.moveTo(x - offset.left, y - offset.top);
							} else {
								ctx.lineTo(x - offset.left, y - offset.top);
							}
						}
						
						if (this.isOldIE) {
						    ctx.fillStyle = '';
							ctx.fill();
							var fill = $('fill', myCanvas).get(0);
							fill.color = '';
							fill.src = element.src;
							fill.type = 'tile';
							fill.alignShape = false;
						}else{
						    var pattern = ctx.createPattern(imageObj, "repeat");
							ctx.fillStyle = pattern;
							ctx.fill();
							var dataurl = canvas.toDataURL("image/png");
							var xhr = new XMLHttpRequest();
							xhr.open('POST', '<?php echo base_url(); ?>admin/projects/saveCroppedImage', false);
							xhr.setRequestHeader("Content-type", "application/x-www-form-urlencoded");
							var files = dataurl;
							var data = new FormData();
							var myprod = $("#pid").val();
							data = 'image=' + files;
							xhr.send(data);
							if (xhr.status === 200) {
								//console.log(xhr.responseText);
								//imageObj.src = "shipbuilding-50.jpg";
								//$('#myimg').css({'box-shadow': '0 0 10px black'});
								$('#previewImage').append('<img class="img-responsive center-block" src="<?php echo base_url(); ?>uploads/cropedImages/' + xhr.responseText + '.jpg"/>');
							}
						 }
						 // Remove first canvas and capture final image
						 $("#myCanvas").remove();
						 await captureFinalImage(deckInput, blockInput, sideInput);
					}, 2000);
				});
		   });
	});
</script>
<script>	


function captureImage(){
       var captureArea = $("#capture-div"), 
	   capturedData;
	   
	   /* capture div */
	   html2canvas(captureArea, {
		 allowTaint: true,
		 useCORS: true,
		 taintTest: false,
		 onrendered: function (canvas) {   
		   $("#previewImage").append(canvas);
				capturedData = canvas;
				var data = canvas.toDataURL('image/png');
				var image = new Image();
				image.src = data;
				var output = data.replace(/^data:image\/(png|jpg);base64,/, "");
			}
	  }); /* capture end */
}

function captureFinalImage(deckInput, blockInput, sideInput){
	   //$("#spotAppenderContainer").css({'width':'1000px', 'height':'600px'});
       var captureArea = $("#spotAppenderContainer"), 
	   capturedData;
	   
	   /* capture final div */
	   html2canvas(captureArea, {
			 allowTaint: true,
			 useCORS: true,
			 taintTest: false,
			 onrendered: function (canvas) {   
					$("#previewImage1").html("").append(canvas);
					capturedData = canvas;
					var data = canvas.toDataURL('image/png');
					var image = new Image();
					image.src = data;
					var output = data.replace(/^data:image\/(png|jpg);base64,/, "");

					$('#img_val1').val('Gourav');
					$('#img_val').val(data);

					 /*** Saving... Final image to Database End ***/
					 var data1 = "deckInput="+deckInput+"&blockInput="+blockInput+"&sideInput="+sideInput+"";
					 var url = "<?php echo base_url(); ?>admin/projects/findProjectsCordinate"; 
					 $.ajax({
						  url: url,
						  cache: false,
						  dataType: 'text',
						  data: data1,
						  method: "POST",
						  success: function(response){
							  console.log(response);
							  if(response == "TRUE"){
								  $("#FormForSaveProjectsCordinate").submit();
							  }else{
								  var confirmAlert = confirm("This cordinates already exist, Do you want to update this ?");
								  if (confirmAlert == true) {
										$("#FormForSaveProjectsCordinate").submit();
								  } else {
										$("#btn-Preview-Image").removeAttr("disabled");
										$("#btn-Preview-Image").html("Save");
								  }
							  }
							  //$("#FormForSaveProjectsCordinate").submit();
						  },
						  error: function(response){
							  alert("Error while processing.");
						  },
					});
					/*** Saved Final image to Database End ***/
			}
	  }); /* capture final end */
}
</script>

<style>
/* .wrapper{width:847px;margin:20px auto} */
.preview{border:1px solid #000;width:847px;height:362px;overflow:hidden}

</style>


	<div class="content-container">
            <div class="container-fluid">
                 <div class="row">	
					 <h3>Scaffold Positioning </h3>
					 <p class="capitalize"><span class="glyphicon glyphicon-list-alt"></span> <?php echo $project['name']; ?></p>
					 
					 
                      <!-- Form Start -->					
					   <!-- Form Start -->					
					  <?php //echo form_open('admin/projects/saveProjectsCordinate', array("id"=> "FormForSaveProjectsCordinate", "class"=> "form-inline")); ?>  
					<form action="<?php echo base_url(); ?>admin/projects/saveProjectsCordinate" id="FormForSaveProjectsCordinate" class="form-inline" method="post"> 
							<div class="row" >
								 <div class="form-group col-sm-2">
									<label for="deck" style="display:block;">Deck:</label>
									<select required="true" name="deck" class="form-control input-sm cordinatesDropDown" style="width: 100%;">
										  <option value="">select</option>
										  <option <?php if(isset($_SESSION['deck']) AND $_SESSION['deck'] == 100){ echo "selected"; } ?>>100</option>
										  <option <?php if(isset($_SESSION['deck']) AND $_SESSION['deck'] == 200){ echo "selected"; } ?>>200</option>
										  <option <?php if(isset($_SESSION['deck']) AND $_SESSION['deck'] == 375){ echo "selected"; } ?>>375</option>
										  <option <?php if(isset($_SESSION['deck']) AND $_SESSION['deck'] == 400){ echo "selected"; } ?>>400</option>
										  <option <?php if(isset($_SESSION['deck']) AND $_SESSION['deck'] == 500){ echo "selected"; } ?>>500</option>
									</select>
								  </div>
								   <div class="form-group col-sm-2">
									<label for="block" style="display:block;">Block:</label>
									<select required="true" name="block" class="form-control input-sm cordinatesDropDown" style="width: 100%;">
										  <option value="">select</option>
										  <option <?php if(isset($_SESSION['block']) AND $_SESSION['block'] == 100){ echo "selected"; } ?>>100</option>
										  <option <?php if(isset($_SESSION['block']) AND $_SESSION['block'] == 200){ echo "selected"; } ?>>200</option>
										  <option <?php if(isset($_SESSION['block']) AND $_SESSION['block'] == 375){ echo "selected"; } ?>>375</option>
										  <option <?php if(isset($_SESSION['block']) AND $_SESSION['block'] == 400){ echo "selected"; } ?>>400</option>
										  <option <?php if(isset($_SESSION['block']) AND $_SESSION['block'] == 500){ echo "selected"; } ?>>500</option>
									</select>
								  </div>
								  <div class="form-group col-sm-2">
									<label for="side" style="display:block;">Side:</label>
									<select required="true" name="side" id="sideDropDown" class="form-control input-sm cordinatesDropDown" style="width: 100%;">
										  <option value="">select</option>
										  <option <?php if(isset($_SESSION['side']) AND $_SESSION['side'] == 100){ echo "selected"; } ?>>100</option>
										  <option <?php if(isset($_SESSION['side']) AND $_SESSION['side'] == 200){ echo "selected"; } ?>>200</option>
										  <option <?php if(isset($_SESSION['side']) AND $_SESSION['side'] == 375){ echo "selected"; } ?>>375</option>
										  <option <?php if(isset($_SESSION['side']) AND $_SESSION['side'] == 400){ echo "selected"; } ?>>400</option>
										  <option <?php if(isset($_SESSION['side']) AND $_SESSION['side'] == 500){ echo "selected"; } ?>>500</option>
									</select>
								  </div>
								  <div class="form-group col-sm-2">
										<button style="margin-top: 18px;" class="btn btn-primary btn-sm" id="crop" old-id="btn-Preview-Image" type="button"><span class="glyphicon glyphicon-floppy-save"></span> Save</button>
										
										<!--
										<button style="margin-top: 18px;" class="btn btn-danger btn-sm" id="reset-canvas" old-id="btn-Preview-Image" type="button"><span class="glyphicon glyphicon-retweet"></span> Reset</button>
										-->
								   </div>
								   
							  </div>
							  <input type="hidden" name="img_val" id="img_val" value="aaaaaaaaaaa">
							  <input type="hidden" name="img_val1" id="img_val1" value="dddddddddddddddd">
							  <input type="hidden" name="projectId" id="projectId" value="<?php echo $project['projectId']; ?>">
							  <input type="hidden" value="Submit"/>
					    </form>
					   <?php //echo form_close(); ?>
					   <!-- Form End -->		
		         </div>
            </div>
			<hr>
			<section class="wrapper">
				<div id="spotAppenderContainer">
					<div id="capture-div">
						<canvas width="633" height="296" id="myCanvas"></canvas>
						<div id="previewImage"></div>  <!---Croped part--->
						

						<div id="oldposx" style="display:none;"></div>
						<div id="oldposy" style="display:none;"></div>
						<div id="posx" style="display:none;"></div>
						<div id="posy" style="display:none;"></div>
					</div>
					
				</div>
			</section>     
			
			<!--=======Full croped part==--->
			<div class="container-fluid">			
				<div class="row wrapper">
					<div id="previewImage1"></div>
				</div>
			</div>
			<!--==============================--->
	</div>

<!--Footer Include-->
<?php $this->load->view('admin/includes/common-footer'); ?>
<!--/Footer Include-->