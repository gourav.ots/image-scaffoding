<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Units extends CI_Controller {
	
	public function __construct(){
		parent::__construct();
	}
	
	public function add($id=null)
	{
		if ($this->input->server('REQUEST_METHOD') == 'POST'){
			$this->load->library('form_validation');
			$this->form_validation->set_error_delimiters('<span class="text-danger">', '</span>');
			$this->form_validation->set_rules('name', 'Unit Name', 'required');
			$this->form_validation->set_rules('type', 'Unit Type', 'required');
			if ($this->form_validation->run() == FALSE)
            {
                 $this->load->view('admin/units/add');
            }else{
				 $data = array(
				              'name' => $this->input->post('name'),
							  'type' => $this->input->post('type'),
							  'added'=> date("Y-m-d H:i:s")
				         );
				 $this->db->insert('units', $data);		 
				 $this->session->set_flashdata('message','<div class="alert alert-success"><b>Congratulation</b> Successfully saved</div>');
				 redirect('admin/units/listing');
			}	
		}else{
			$this->load->view('admin/units/add');
		}

	}
	
		
	public function update()
	{
		$type_decrypted_key = str_replace(" ", '+', $this->input->get('id'));
		$id = mc_decrypt($type_decrypted_key, ENCRYPTION_KEY);
		
		if ($this->input->server('REQUEST_METHOD') == 'POST'){
			$this->load->library('form_validation');
			$this->form_validation->set_error_delimiters('<span class="text-danger">', '</span>');
			$this->form_validation->set_rules('name', 'Unit Name', 'required');
			$this->form_validation->set_rules('type', 'Unit Type', 'required');
			if ($this->form_validation->run() == FALSE)
            {
                 $this->load->view('admin/units/add');
            }else{
				 $data = array(
				              'name' => $this->input->post('name'),
							  'type' => $this->input->post('type'),
							  'updated'=> date("Y-m-d H:i:s")
				         );
				 $this->db->where('id', $id);		 		 
				 $this->db->update('units', $data);	
				 
				 $this->session->set_flashdata('message','<div class="alert alert-success"><b>Congratulation</b> Successfully updated</div>');
				 redirect('admin/units/listing');
			}	
		}else{
				$data['result'] = $this->db->get_where('units', array('id'=> $id))->row_array();	
				$this->load->view('admin/units/add', $data);
		}

	}
    
	public function listing()
	{
		  
		  $this->load->library('pagination');
		
		  $config['base_url'] = base_url().'admin/units/listing/';
		  $config['total_rows'] =  $this->db->get_where('units', array('status'=> 1))->num_rows();
		  $config['per_page'] = 15;
		  $config['page_query_string'] = TRUE;
		  $config['query_string_segment'] = 'page';
		  
		  $page = (isset($_GET['page'])) ? $_GET['page'] : 0;
		  
		  $this->pagination->initialize($config);
		  
		  $data["pagination"] = $this->pagination->create_links();
		  
		  $data['result'] =  $this->db->get_where('units', array('status'=> 1), $config["per_page"], $page)->result_array();
		  $this->load->view('admin/units/list', $data);
	}
	
	public function delete(){
		$type_decrypted_key = str_replace(" ", '+', $this->input->get('id'));
		$id = mc_decrypt($type_decrypted_key, ENCRYPTION_KEY);
		
		$data = array(
			'status'=> '0'
		);
	    $this->db->where('id', $id);		 		 
	    $this->db->update('units', $data);	
		
		$this->session->set_flashdata('message','<div class="alert alert-danger"><b>Congratulation</b> Successfully deleted</div>');
		redirect('admin/units/listing');
	}


}
