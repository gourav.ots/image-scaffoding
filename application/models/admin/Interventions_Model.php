<?php  defined('BASEPATH') OR exit('No direct script access allowed');

class Interventions_Model extends CI_Model {
	
    public function update_save($images){
		$id_decrypted_key = str_replace(" ", '+', $_POST['id']);
		$id_dec = mc_decrypt($id_decrypted_key, ENCRYPTION_KEY);
		$id = $id_dec;
		$data = array(
                    'name'=> $this->input->post('name'),
                    'description'=> $this->input->post('description'),
		            'images'=> $images,
			        'video_url' => $this->input->post('video_url')
		);
		$this->db->where('id', $id);
		$this->db->update('prevention_interventions', $data);
		
	}	
	
}	
		