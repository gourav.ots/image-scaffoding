<!--Header Include-->
<?php $this->load->view('admin/includes/common-header'); ?>
<!--/Header Include-->
 
    <?php if(!isset($result)){ ?>
		<div class="content-container">
				<div class="container-fluid">
					 <div class="row">	
						 <h3>Add Units</h3>
					 </div>
				</div>
				<div class="bd-example">
					<?php echo form_open('admin/units/add', array('data-parsley-validate'=> 'validate', 'autocomplete'=> 'off')); ?>    
						  <div class="form-group">
							<label>Unit Name <span class="text-danger">*</span></label>
							<input type="text" name="name" value="<?php echo set_value('name'); ?>" class="form-control" placeholder="Unit Name" required="true">
							<?php echo form_error('name'); ?>
						  </div>
						  
						  <div class="form-group">
							<label>Unit Type <span class="text-danger">*</span></label>
							<select name="type" class="form-control" required="true">
							     <option value="">Please Select</option>
								 <option value="1">Operation Room</option>
								 <option value="2">Others</option>
							</select>
							<?php echo form_error('type'); ?>
						  </div>
						  
						  <button type="submit" class="btn btn-primary">Submit</button>
					<?php echo form_close(); ?><!-- /form -->
				</div>
		</div>
	<?php }else{ ?>	
	    <div class="content-container">
				<div class="container-fluid">
					 <div class="row">	
						 <h3>Update Units</h3>
					 </div>
				</div>
				<div class="bd-example">
					<?php echo form_open('admin/units/update?id='.mc_encrypt($result['id'],ENCRYPTION_KEY), array('data-parsley-validate'=> 'validate', 'autocomplete'=> 'off')); ?>    
						  <div class="form-group">
							<label>Unit Name <span class="text-danger">*</span></label>
							<input type="text" name="name" value="<?php echo $result['name']; ?>" class="form-control" placeholder="Unit Name" required="true">
							<?php echo form_error('name'); ?>
						  </div>
						  
						  <div class="form-group">
							<label>Unit Type <span class="text-danger">*</span></label>
							<select name="type" class="form-control" required="true">
							     <option value="">Please Select</option>
								 <option value="1" <?php if($result['type']==1){echo 'selected';}; ?>>Operation Room</option>
								 <option value="2" <?php if($result['type']==2){echo 'selected';}; ?>>Others</option>
							</select>
							<?php echo form_error('type'); ?>
						  </div>
						  
						  <button type="submit" class="btn btn-primary">Submit</button>
					<?php echo form_close(); ?><!-- /form -->
				</div>
		</div>
	
    <?php } ?>	

<!--Footer Include-->
<?php $this->load->view('admin/includes/common-footer'); ?>
<!--/Footer Include-->