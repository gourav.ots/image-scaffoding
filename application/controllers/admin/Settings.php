<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Settings extends CI_Controller {
	
	public function __construct(){
		parent::__construct();
	}
	
	public function index()
	{
		  if($this->input->post()){
			   $data = array(
						     'crop_default_width'=> $this->input->post('crop_default_width'),
						     'crop_default_height'=> $this->input->post('crop_default_height')
					   );
				
				$this->db->where('id', 1);				
			    $this->db->update('settings', $data);	 
				$this->session->set_flashdata('message','<div class="alert alert-success alert-notification"><b>Congratulation</b> Settings successfully updated.</div>');
				redirect('admin/settings', 'reload');	
			    
				$data['result'] =  $this->db->get_where('settings')->row_array();
			    $this->load->view('admin/settings/update', $data);				
		  }else{
			  $data['result'] =  $this->db->get_where('settings')->row_array();
			  $this->load->view('admin/settings/update', $data);
		  }
		  
	}
}
