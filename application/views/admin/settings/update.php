<!--Header Include-->
<?php $this->load->view('admin/includes/common-header'); ?>
<!--/Header Include-->
<style>
.validatr-message {
    margin-top: 1px;
}
</style>	


	<div class="content-container">
		<div class="container-fluid">
            <?php echo form_open('admin/settings'); ?>    
			    <div class="row">
					<h3>Settings</h3><br>				
					<div class="col-sm-6">
						<div class="row">
							  <div class="form-group" style="margin-bottom: 30px;">
								<label>Crop Default Width <span class="text-danger">*</span></label>
								<input type="number" name="crop_default_width" class="form-control" placeholder="Crop Default Width(in Px)" value="<?php echo $result['crop_default_width']; ?>" required="true">
							  </div>
							  <div class="form-group" style="margin-bottom:30px;">
								<label>Crop Default Height <span class="text-danger">*</span></label>
								<input type="number" name="crop_default_height" class="form-control" placeholder="Crop Default Height(in Px)" value="<?php echo $result['crop_default_height']; ?>" required="true">
							  </div>
						</div>	
						<br>
						<div class="row">	
							<button type="submit" class="btn btn-primary btn-sm pull-right"><span class="glyphicon glyphicon-floppy-save"></span> Update</button>
						</div>							
					</div>
				</div>
			<?php echo form_close(); ?>
		</div>	
	</div>

<!--Footer Include-->
<?php $this->load->view('admin/includes/common-footer'); ?>
<!--/Footer Include-->