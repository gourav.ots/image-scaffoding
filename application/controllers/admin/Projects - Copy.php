<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Projects extends CI_Controller {
	
	public function __construct(){
		parent::__construct();
		$this->load->library('session');
		$this->load->helper('url');
	}
	
	public function index()
	{
		  $this->load->library('pagination');
		  $config['base_url'] = base_url().'admin/projects/';
		  $config['total_rows'] =  $this->db->get_where('projects', array('status'=> 1))->num_rows();
		  $config['per_page'] = 15;
		  $config['page_query_string'] = TRUE;
		  $config['query_string_segment'] = 'page';
		  
		  $page = (isset($_GET['page'])) ? $_GET['page'] : 0;
		  
		  $this->pagination->initialize($config);
		  
		  $data["pagination"] = $this->pagination->create_links();
		  
		  $data['result'] =  $this->db->get_where('projects', array('status'=> 1), $config["per_page"], $page)->result_array();
		  $this->load->view('admin/projects/listing', $data);
	}
	
	public function view($projectId)
	{
		  $data['project'] = $this->db->get_where('projects', array('projectId'=> $projectId))->row_array();
		  $data['settings'] = $this->db->get_where('settings')->row_array();
		  $this->load->view('admin/projects/view', $data);
	}
	
	public function saveProjectsCordinate(){
		 $current_timestamp = strtotime("now");
		 
		 $image_val = $_POST['img_val']; //image name
		 $encodedData = str_replace(' ','+',$image_val);
		 $decocedData = base64_decode($encodedData);
		 $new_img_name = 'uploads/cordinateImages/'.$current_timestamp.'_img.png'; // new image name
		 
		 file_put_contents($new_img_name, $decocedData); //saving file into folder
		 
		 $findByCordinates = $this->db->get_where('projectscordinates', array("deck" => $_POST['deck'], "block"=> $_POST['block'], "side"=> $_POST['side']))->row_array();
		 
		 if(empty($findByCordinates)){
			 echo "tets 11";
			 $data = array(
				  "projectId" => $_POST['projectId'],
				  "image" => $new_img_name,
				  "deck" => $_POST['deck'],
				  "block" => $_POST['block'],
				  "side" => $_POST['side'],
				  "x1" => $_POST['x1'],
				  "x2" => $_POST['x2'],
				  "y1" => $_POST['y1'],
				  "y2" => $_POST['y2']
			  ); 
			  $this->db->insert('projectscordinates', $data);
			  
			  $_SESSION['deck'] = "";
			  $_SESSION['block'] = "";
			  $_SESSION['side'] = "";
			  $_SESSION['x1'] = "";
			  $_SESSION['x2'] = "";
			  $_SESSION['y1'] = "";
			  $_SESSION['y2'] = "";
			  
			  $this->session->set_flashdata('message','<div class="alert alert-success alert-notification"><b>Congratulation</b> Successfully saved.</div>');
			  redirect($_SERVER['HTTP_REFERER'],'refresh'); 
			  
		 }else{
			  $_SESSION['deck'] = $_POST['deck'];
			  $_SESSION['block'] = $_POST['block'];
			  $_SESSION['side'] = $_POST['side'];
			  $_SESSION['x1'] = $_POST['x1'];
			  $_SESSION['x2'] = $_POST['x2'];
			  $_SESSION['y1'] = $_POST['y1'];
			  $_SESSION['y2'] = $_POST['y2'];
			  
			  $this->session->set_flashdata('message','<div class="alert alert-danger alert-notification"><b>Sorry</b> This cordinates already exist.</div>');
			  redirect($_SERVER['HTTP_REFERER'],'refresh'); 
		 }
	}
	
	public function findProjectsCordinate(){
		 $findByCordinates = $this->db->get_where('projectscordinates', array("deck" => $_POST['deck'], "block"=> $_POST['block'], "side"=> $_POST['side']))->row_array();
		 
		 if(empty($findByCordinates)){
			 echo TRUE;
		 }else{
			 echo FALSE;
		 }	 
	}	 
	
	public function delete(){
		$type_decrypted_key = str_replace(" ", '+', $this->input->get('id'));
		$id = mc_decrypt($type_decrypted_key, ENCRYPTION_KEY);
		
		$data = array(
			'status'=> '0'
		);
	    $this->db->where('id', $id);		 		 
	    $this->db->update('units', $data);	
		
		$this->session->set_flashdata('message','<div class="alert alert-danger"><b>Congratulation</b> Successfully deleted</div>');
		redirect('admin/units/listing');
	}


}
