<?php
   defined('BASEPATH') OR exit('No direct script access allowed');
   if(!$this->session->userdata('username')){
	  // redirect('admin/users/logout');
   }
?>


<!DOCTYPE html >
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
	<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
	<title>Image Scaffholding</title>
	<meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=no" /> 
	<link rel="stylesheet" type="text/css" href="<?php echo base_url(); ?>resources/css/bootstrap.min.css" media="all"/>
	<link href="<?php echo base_url(); ?>resources/css/simple-sidebar.css" rel="stylesheet">
	<link rel="stylesheet" type="text/css" href="<?php echo base_url(); ?>resources/css/layout.css" media="all"/>
	
	<script src="<?php echo base_url(); ?>resources/js/jquery.min.js"></script>
	<script src="<?php echo base_url(); ?>resources/js/bootstrap.min.js"></script>
	
	<script>
		$(document).ready(function(){
			
			$("#menu-toggle").click(function(e) {
				e.preventDefault();
				$("#wrapper").toggleClass("toggled");
			});
			$('.tree-dropdown-anker').click(function(e) {
				e.preventDefault();
				var id = $(this).attr('id');
				$('.'+id+'-list').slideToggle();
			});
			
		});
		$(document).ready(function(){
			  setTimeout(function(){ 
				 $('.alert.alert-notification').slideUp('slow');
			  }, 6000);
         });
     </script>
</head>
<body>

    <div id="loading-div"></div>


    <div id="wrapper">
		<!--Left Nav Start-->
		<?php $this->load->view('admin/includes/left-nav'); ?>
		<!--/Left Nav-->
	    <div class="row element-to-hide">	
			<div class="col-md-12">	
				<div class="red-top">
				  <a href="#menu-toggle" id="menu-toggle">
					 <span class="glyphicon glyphicon-align-justify"></span>
				   </a>
				  <!--<div class="logout">
					<h5><a href="#" >Logout</a></h5>
				  </div>-->
				</div>
			 </div>
	    </div>
		
		<?php if($this->session->flashdata('message')){
				echo $this->session->flashdata('message');
		}?>
		