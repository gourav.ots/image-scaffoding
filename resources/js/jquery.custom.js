	// Login Page Background
	$(function() {   
		var theWindow        = $(window),
			$bg              = $("#login_bg"),
			aspectRatio      = $bg.width() / $bg.height();
			
		function resizeBg() {
			
			if ( (theWindow.width() / theWindow.height()) < aspectRatio ) {
				$bg
					.removeClass()
					.addClass('login_bgheight');
			} else {
				$bg
					.removeClass()
					.addClass('login_bgwidth');
			}
						
		}
									
		theWindow.resize(function() {
			resizeBg();
		}).trigger("resize");
		
	});
	
	
	
	
	//Circle Content Function
	$('#RedFlag').click(function() {
		$('#red-flag-qus').slideDown();
		$('#ScottTriggers-qus').hide();
	});
	$('#ScottTriggers').click(function() {
		$('#ScottTriggers-qus').slideDown();
		$('#red-flag-qus').hide();
	});
	
	
	
	
	$('.circle_option').click(
        function() {
			$('.circle_option').removeClass('able').addClass('disable');
			$(this).removeClass('disable').addClass('able');
	});
	
	function goBack() {
		window.history.back();
    }