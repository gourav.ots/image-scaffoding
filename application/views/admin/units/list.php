<!--Header Include-->
<?php $this->load->view('admin/includes/common-header'); ?>
<!--/Header Include-->



	<div class="content-container">
            <div class="container-fluid">
                 <div class="row">	
					 <h3>Listings</h3>
		         </div>
            </div>
			<div class="bd-example">
			            
						<div class = "table-responsive">
							   <table class = "table table-bordered table-striped">
								  
								  <thead>
									 <tr>
										<th>Name</th> 
										<th>Type</th>										
										<th>Added Date</th>
									 </tr>
								  </thead>
								  
								  <tbody>
								        <?php foreach($result as $row){ ?>
											 <tr>
												
												<td><?php echo $row['name']; ?></td>
												<td><?php if($row['type']==1){
															  echo 'Operation Room';
														  }else{
															  echo 'Others';
														  }	  ; 
													 ?>
												</td>
												<td><?php echo $row['added']; ?></td>
												
											 </tr>
										<?php } ?>	 
								  </tbody>
								  
							   </table>
							</div>  
							<nav aria-label="Page navigation">
							     <ul class="pagination">
							         <li class="page-item"><?php echo $pagination ?></li>
							     </ul>
							</nav>	 
			</div>
	</div>

<!--Footer Include-->
<?php $this->load->view('admin/includes/common-footer'); ?>
<!--/Footer Include-->