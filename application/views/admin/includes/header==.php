<?php
defined('BASEPATH') OR exit('No direct script access allowed');
?>


<!DOCTYPE html >
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
	<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
	<title>Wound Login</title>
	<meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=no" /> 
	<link rel="stylesheet" type="text/css" href="<?php echo base_url(); ?>resources/css/bootstrap.css" media="all"/>
	<link rel="stylesheet" type="text/css" href="<?php echo base_url(); ?>resources/css/layout.css" media="all"/>
	
	<script src="<?php echo base_url(); ?>resources/js/jquery.min.js"></script>
	<script src="<?php echo base_url(); ?>resources/js/bootstrap.min.js"></script>
</head>
<body>