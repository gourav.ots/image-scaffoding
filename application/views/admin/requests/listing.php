<!--Header Include-->
<?php $this->load->view('admin/includes/common-header'); ?>
<!--/Header Include-->



	<div class="content-container">
            <div class="container-fluid">
                 <div class="row">	
					 <h3>Projects</h3>
		         </div>
            </div>
			<div class="bd-example">
			            
						<div class = "table-responsive">
							   <table class = "table table-bordered table-striped">
								  
								  <thead>
									 <tr>
										<th>Name</th> 
										<th>Deck</th> 
										<th>Block</th> 
										<th>Side</th> 
										<th>Date</th> 
										<th>Delete</th>		
									 </tr>
								  </thead>
								  
								  <tbody>
								        <?php foreach($result as $row){ ?>
											 <tr>
												<td class="capitalize"><?php echo $row['name']; ?></td>
												<td class="capitalize"><?php echo $row['deck']; ?></td>
												<td class="capitalize"><?php echo $row['block']; ?></td>
												<td class="capitalize"><?php echo $row['side']; ?></td>
												<td class="capitalize"><?php echo $row['created']; ?></td>
												<td><a  onclick="return confirm('Are you really want to delete this?')"  href="<?php base_url(); ?>requests/delete?id=<?php echo mc_encrypt($row['projectCordinateRequestId'],ENCRYPTION_KEY); ?>">Delete</a></td>
											 </tr>
										<?php } ?>	 
								  </tbody>
								  
							   </table>
							</div>  
							<nav aria-label="Page navigation">
							     <ul class="pagination">
							         <li class="page-item"><?php echo $pagination ?></li>
							     </ul>
							</nav>	 
			</div>
	</div>

<!--Footer Include-->
<?php $this->load->view('admin/includes/common-footer'); ?>
<!--/Footer Include-->